# my global config
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - monitor.den:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  - '/etc/prometheus/alerts/*.yml'

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'
    static_configs:
      - targets:
        - localhost:9090

  - job_name: 'alertmanager'
    static_configs:
      - targets:
        - localhost:9093

  - job_name: 'node'
    static_configs:
      # Bare metal
      - targets:
        - dogee.data.den:9100
        - drolf-rpi.data.den:9100
        - rpi-k8s-01.data.den:9100
        - rpi-k8s-02.data.den:9100
        - rpi-k8s-03.data.den:9100
        - rpi-lxd-01.data.den:9100
        - rpi-lxd-02.data.den:9100
        labels:
          group: hw
      # Remote VMs
      - targets:
        - 'tigerfox.isgeek.info:9273'
        labels:
          group: vm
      # LXC containers
      - targets:
        - build.data.den:9100
        - dns-01.data.den:9100
        - dns-02.data.den:9100
        - docker.data.den:9100
#        - kodi.data.den:9100
        - mediabox.data.den:9100
        - monitor.data.den:9100
        - nas.data.den:9100
        - pihole-01.data.den:9100
        - pihole-02.data.den:9100
        - prolink-overlay.data.den:9100
        - puppet5.data.den:9100
        - puppet-01.data.den:9100
        - puppet-02.data.den:9100
        - smtp.data.den:9100
        - torrent.data.den:9100
        labels:
          group: lxd
    metric_relabel_configs:
      - source_labels: [instance]
        regex: '(.+)\.data\.(.+):\d+'
        replacement: '$1.$2'
        target_label: fqdn

  - job_name: 'pihole'
    static_configs:
      - targets:
        - pihole-01.data.den:9617
        - pihole-02.data.den:9617
    metric_relabel_configs:
      - source_labels: [instance]
        regex: '(.+)\.data\.(.+):\d+'
        replacement: '$1.$2'
        target_label: fqdn
      - regex: 'hostname'
        action: labeldrop

  # Blackbox monitoring

  - job_name: 'blackbox-ping'
    metrics_path: /probe
    params:
      module: [icmp]
    static_configs:
      - targets:
        # VLAN 20
        - router.mgmt.den
        - switch.mgmt.den
        - cap1.mgmt.den
        - mikrotik-test.mgmt.den
        - switch-poe.mgmt.den
        # VLAN gateways
        - router.data.den  # VLAN 21
        - gw-vlan30.den
        - gw-vlan31.den
        - gw-vlan32.den
        - gw-vlan40.den
        - gw-vlan41.den
        - gw-vlan42.den
        - gw-vlan43.den
        - gw-vlan44.den
        - gw-vlan45.den
        # Bare metal
        - dogee.mgmt.den
        - drolf-rpi.mgmt.den
        - rpi-k8s-01.mgmt.den
        - rpi-k8s-02.mgmt.den
        - rpi-k8s-03.mgmt.den
        - rpi-lxd-01.mgmt.den
        - rpi-lxd-02.mgmt.den
        # LXCs
        - build.den
        - dns-01.den
        - dns-02.den
        - docker.den
#        - kodi.den
        - mediabox.den
        - monitor.den
        - nas.den
        - pihole-01.den
        - pihole-02.den
        - prolink-overlay.den
        - puppet-01.den
        - puppet-02.den
        - smtp.den
        - torrent.den
        # VIPs
        - puppet.den
        # Connect Plus
        - 10.230.7.161  # router in the building
        # Public
        - 1.1.1.1
        - 9.9.9.9
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 127.0.0.1:9115

  - job_name: 'blackbox-http'
    metrics_path: /probe
    params:
      module: [http_2xx]
    static_configs:
      - targets:
        # .den services
        - http://home-assistant.den
        - http://monitor.den
        - http://mediabox.den
        - http://torrent.den
        - http://pihole-01.den
        - http://pihole-02.den
        # Public
        - https://google.cz
        - https://google.com
        - https://proton.me
        - https://seznam.cz
        - https://taiku.cz
        - https://bw.taiku.cz
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 127.0.0.1:9115

  - job_name: 'snmp'
    metrics_path: /snmp
    params:
      module: [mikrotik]
    static_configs:
      - targets:
        # intentionally no DNS
        - 10.7.21.1
        - 10.7.21.2
        - 10.7.21.3
        - 10.7.21.4
        - 10.7.21.5
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 127.0.0.1:9116

  - job_name: 'apcupsd'
    static_configs:
      - targets:
        - drolf-rpi.data.den:9162
    metric_relabel_configs:
      - source_labels: [instance]
        regex: '(.+)\.data\.(.+):\d+'
        replacement: '$1.$2'
        target_label: fqdn

  - job_name: 'loki'
    static_configs:
      - targets:
        - localhost:3100
    metric_relabel_configs:
      - source_labels: [instance]
        regex: '(.+)\.data\.(.+):\d+'
        replacement: '$1.$2'
        target_label: fqdn

  - job_name: 'promtail'
    static_configs:
      - targets:
        - localhost:9080
    metric_relabel_configs:
      - source_labels: [instance]
        regex: '(.+)\.data\.(.+):\d+'
        replacement: '$1.$2'
        target_label: fqdn

  - job_name: 'lxd'
    metrics_path: /1.0/metrics
    scheme: 'https'
    static_configs:
      - targets:
        - rpi-lxd-01.data.den:8443
        - rpi-lxd-02.data.den:8443
    tls_config:
      cert_file: 'tls/lxd.crt'
      key_file: 'tls/lxd.key'
      # LXD has its own CA, and I'd have to copy-paste both CA certificates here and verify.
      # Plus I'd have to separate the jobs for each machine, let's try this way first.
      insecure_skip_verify: true
